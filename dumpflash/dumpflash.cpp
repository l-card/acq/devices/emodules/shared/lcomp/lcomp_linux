



// !!!!!!!!!!!!!!!!!!!!!! DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!! DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!! DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!! DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!! DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!

// code commented out is DANGER - overwrite flash !!!!!!!!!!!!!!!!!!!!!!!!!!!

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <dlfcn.h>
#include <pthread.h>

#include <iostream>

#include <math.h>

using namespace std;

#include <termios.h>

static struct termios stored_settings,new_settings;
static int peek_character = -1;

static int ctrlc = 0;

void reset_keypress(void)
{
   ctrlc=1;
   tcsetattr(0,TCSANOW,&stored_settings);
   return;
}


void set_keypress(void)
{
   tcgetattr(0,&stored_settings);

   new_settings = stored_settings;

   /* Disable canonical mode, and set buffer size to 1 byte */
   new_settings.c_lflag &= (~ICANON);
   new_settings.c_lflag &= ~ECHO;
   new_settings.c_lflag &= ~ISIG;
   new_settings.c_cc[VTIME] = 0;
   new_settings.c_cc[VMIN] = 1;

   atexit(reset_keypress);
   tcsetattr(0,TCSANOW,&new_settings);
   return;
}


int kbhit()
{
unsigned char ch;
int nread;

    if (peek_character != -1) return 1;
    new_settings.c_cc[VMIN]=0;
    tcsetattr(0, TCSANOW, &new_settings);
    nread = read(0,&ch,1);
    new_settings.c_cc[VMIN]=1;
    tcsetattr(0, TCSANOW, &new_settings);
    if(nread == 1)
    {
        peek_character = ch;
        return 1;
    }
    return 0;
}

int readch()
{
char ch;

    if(peek_character != -1)
    {
        ch = peek_character;
        peek_character = -1;
        return ch;
    }
    read(0,&ch,1);
    return ch;
}



#define INITGUID

#include "../include/stubs.h"
#include "../include/ioctl.h"
#include "../include/e2010cmd.h"
#include "../include/791cmd.h"
#include "../include/ifc_ldev.h"
#include <errno.h>

typedef IDaqLDevice* (*CREATEFUNCPTR)(ULONG Slot);

CREATEFUNCPTR CreateInstance;


unsigned short *p;
//unsigned long *pl;
unsigned int *pp;

unsigned int *dp;
unsigned int *dpp;

int IrqStep=1024;
int  pages=256;
int  multi=32;
unsigned short complete;


//Att. for board slot numbers!!!!

int main(int argc, char **argv)
{
PLATA_DESCR_U2 pd;
SLOT_PAR sl;

HANDLE hVxd;
void *handle;

char *error;
pthread_t thread1;

   set_keypress();

   if(argc==1)
   {
      cout << "L-7XX simple example (L761,L780,L783,L791,E440,E140,E2010)." << endl;
      cout << "(c) 2007 L-Card." << endl;
      cout << "Usage:" << endl;
      cout << "   client <slot number> <bios name>" << endl;
      cout << "   <slot number> - 0... (if one board installed - 0);" << endl;
      cout << "   <bios name>  - bios name without extension(l761 l780 l783 e2010 e440);" << endl;
      reset_keypress();      
      exit(0);
   }
   if(argc<3)
   {
      cout << "no params - exit!" << endl;
      reset_keypress();
      exit(0);
   }


   handle = dlopen("./liblcomp.so",RTLD_LAZY);
   if(!handle)
   {
      cout << "error open dll!! " << dlerror() << endl;
   }

   CreateInstance =(CREATEFUNCPTR) dlsym(handle,"CreateInstance");
   if((error = dlerror())!=NULL)
   {
      cout << error << endl;
   }

   LUnknown* pIUnknown = CreateInstance(atoi(argv[1]));
         cout << errno << endl;
   if(pIUnknown == NULL) { cout << "CallCreateInstance failed " << endl; reset_keypress(); return 1; }

   cout << "Get IDaqLDevice interface" << endl;
   IDaqLDevice* pI;
   HRESULT hr = pIUnknown->QueryInterface(IID_ILDEV,(void**)&pI);
   if(hr!=S_OK) { cout << "Get IDaqLDevice failed" << endl; reset_keypress();return 1; }
   printf("IDaqLDevice get success \n");
   pIUnknown->Release();
   cout << "Free IUnknown" << endl;

   cout << "OpenLDevice Handle" << hex << (hVxd=pI->OpenLDevice()) << endl;

   cout << endl << "Slot parameters" << endl;
   pI->GetSlotParam(&sl);

   cout << "Base    " << hex << sl.Base << endl;
   cout << "BaseL   " << sl.BaseL << endl;
   cout << "Mem     " << sl.Mem << endl;
   cout << "MemL    " << sl.MemL << endl;
   cout << "Type    " << sl.BoardType << endl;
   cout << "DSPType " << sl.DSPType << endl;
   cout << "Irq     " << sl.Irq << endl;

   cout << "Load Bios " << pI->LoadBios(argv[2]) << endl;
   cout << "Plata Test " << pI->PlataTest() << endl;

   cout << endl << "Read FLASH" << endl;

   pI->ReadPlataDescr(&pd); // fill up properties
   switch (sl.BoardType)
   {
   case PCIA:
   case PCIB:
   case PCIC:  {
                  cout << "SerNum       " << pd.t1.SerNum << endl;
                  cout << "BrdName      " << pd.t1.BrdName << endl;
                  cout << "Rev          " << pd.t1.Rev << endl;
                  cout << "DspType      " << pd.t1.DspType << endl;
                  cout << "IsDacPresent " << (int)pd.t1.IsDacPresent << endl;
                  cout << "Quartz       " << dec << pd.t1.Quartz << endl;
                  for(int i=0;i<8;i++) cout << "KoefADC[" << i << "] " << pd.t1.KoefADC[i] << endl;
                  for(int i=0;i<4;i++) cout << "KoefDAC[" << i << "] " << pd.t1.KoefDAC[i] << endl;

// DANGER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
// fill pd with some data
/*
		  snprintf(pd.t1.SerNum,9,"3R386436"); // your serial num (4R347 for example)
		  snprintf(pd.t1.BrdName,5,"L780"); // boad name
		  pd.t1.Rev = 'C'; // rev a b c
		  snprintf(pd.t1.DspType,5,"2185"); // dsp type 2184 2185 2186
		  pd.t1.Quartz = 14745600; // for L780 L761 - 14745600, L783 - 20000000
		  pd.t1.IsDacPresent = 1; // 0 or 1
                  for(int i=0;i<4;i++) pd.t1.KoefADC[i] = 0;
                  for(int i=4;i<8;i++) pd.t1.KoefADC[i] = 32767;
                  for(int i=0;i<2;i++) pd.t1.KoefDAC[i] = 0;
                  for(int i=2;i<4;i++) pd.t1.KoefDAC[i] = 10000;                  

		  pI->WritePlataDescr(&pd,1); // write
*/
		  
               } break;
   case E140:  {
                  cout << "SerNum       " << pd.t5.SerNum << endl;
                  cout << "BrdName      " << pd.t5.BrdName << endl;
                  cout << "Rev          " << pd.t5.Rev << endl;
                  cout << "DspType      " << pd.t5.DspType << endl;
                  cout << "IsDacPresent " << hex << (int)pd.t5.IsDacPresent << endl;
                  cout << "Quartz       " << dec << pd.t5.Quartz << endl;
               } break;
   case E440:  {
                  cout << "SerNum       " << pd.t4.SerNum << endl;
                  cout << "BrdName      " << pd.t4.BrdName << endl;
                  cout << "Rev          " << pd.t4.Rev << endl;
                  cout << "DspType      " << pd.t4.DspType << endl;
                  cout << "IsDacPresent " << hex << pd.t4.IsDacPresent << endl;
                  cout << "Quartz       " << dec << pd.t4.Quartz << endl;
               } break;
   case E2010B:
   case E2010: {
                  cout << "SerNum       " << pd.t6.SerNum << endl;
                  cout << "BrdName      " << pd.t6.BrdName << endl;
                  cout << "Rev          " << pd.t6.Rev << endl;
                  cout << "DspType      " << pd.t6.DspType << endl;
                  cout << "IsDacPresent " << pd.t6.IsDacPresent << endl;
                  cout << "Quartz       " << dec << pd.t6.Quartz << endl;
               } break;

   case L791: {
                  cout << "SerNum       " << pd.t3.SerNum << endl;
                  cout << "BrdName      " << pd.t3.BrdName << endl;
                  cout << "Rev          " << pd.t3.Rev << endl;
                  cout << "DspType      " << pd.t3.DspType << endl;
                  cout << "IsDacPresent " << pd.t3.IsDacPresent << endl;
                  cout << "Quartz       " << dec << pd.t3.Quartz << endl;
               } break;
   case E154: {
                  cout << "SerNum       " << pd.t7.SerNum << endl;
                  cout << "BrdName      " << pd.t7.BrdName << endl;
                  cout << "Rev          " << pd.t7.Rev << endl;
                  cout << "DspType      " << pd.t7.DspType << endl;
                  cout << "IsDacPresent " << pd.t7.IsDacPresent << endl;
                  cout << "Quartz       " << dec << pd.t7.Quartz << endl;
               } break;

   }



   
   cout << endl << "Press any key" << dec << endl;
   readch();


   pI->CloseLDevice();

   reset_keypress();

   if(handle) dlclose(handle);
   return 0;
}
